import main.Main;
import org.testng.annotations.Test;
import page.wordpress_objects.WordpressMainPage_Component;
import page.wordpress_objects.WordpressReportsPage_Component;
import steps.waits.Methods;
import java.io.IOException;


public class WordpressReportsTest extends Main {

    @Test
    public void wordpressDemo2() throws InterruptedException, IOException {

        Methods.openPage("http://51.83.43.62/wordpress/sklep/");

        WordpressMainPage_Component wordpressMain = new WordpressMainPage_Component();
        wordpressMain.goToReports();
        wordpressMain.logIn("JanAutomatTrzeci", "training");

        WordpressReportsPage_Component wordpressReports = new WordpressReportsPage_Component();
        wordpressReports.selectPeriod(parameters.getParameter("datePeriod"));
        wordpressReports.exportCSV();
        wordpressReports.getGraphScreenShoot();
    }
}
